﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

//-----------------------------------------------------------------------------
// Copyright 2012-2016 RenderHeads Ltd.  All rights reserverd.
//-----------------------------------------------------------------------------

public class RawImageMapper : MonoBehaviour
{
    public RawImage RawImage;
    public AVProWindowsMediaMovie _movie;
    public Texture2D _defaultTexture;

    void Start()
    {
        if(!RawImage)
        {
            RawImage = BigScreenManager.Instance.VideoScreen.GetComponent<RawImage>();
        }
    }

    void Update()
    {
        bool applied = false;
        if (_movie != null && _movie.MovieInstance != null)
        {
            Texture texture = _movie.OutputTexture;
            if (texture != null)
            {
                ApplyMapping(texture, _movie.MovieInstance.RequiresFlipY);
                applied = true;
            }
        }

        if (!applied)
        {
            ApplyMapping(_defaultTexture, false);
        }
    }

    private void ApplyMapping(Texture texture, bool requiresYFlip)
    {
        if (BigScreenManager.Instance)
        {
            if (!RawImage)
            {
                RawImage = BigScreenManager.Instance.VideoScreen.GetComponent<RawImage>();
            }
            RawImage.texture = texture;
        }
    }

    void OnEnable()
    {
        Update();
    }

    //void OnDisable()
    //{
    //    ApplyMapping(_defaultTexture, false);
    //}
}