﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DataManager : MonoSingleton<DataManager>
{
    List<Task> tasksCompleted;

    void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        tasksCompleted = new List<Task>();
    }
    
    public void RecordActivity(Task _task)
    {
        tasksCompleted.Add(_task);
    }

    public List<Task> ReadActivities()
    {
        return tasksCompleted;
    }
}