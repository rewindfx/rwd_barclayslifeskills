﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;

public class EmailManager : MonoSingleton<EmailManager>
{
    void Awake()
    {
        Instance = this;
    }

    public void SendEmail(List<Task> _tasks)
    {
        if(_tasks.Count == 0)
        {
            Debug.Log("No tasks recorded");
            return;
        }
        string bodyText = "";
        foreach(Task t in _tasks)
        {
            bodyText += t.Type.ToString();
            bodyText += "\n";
            switch (t.Response)
            {
                case ResponseValue.Correct:
                    bodyText += t.CorrectResponse;
                    break;

                case ResponseValue.Almost:
                    bodyText += t.AlmostCorrectResponse;
                    break;

                case ResponseValue.Incorrect:
                    bodyText += t.IncorrectResponse;
                    break;

                case ResponseValue.Unanswered:
                    bodyText += t.UnansweredResponse;
                    break;
            }
            bodyText += "\n\n";
        }
        bodyText += "Thank you etc. \n Signed Barclays";

        MailMessage mail = new MailMessage();

        mail.From = new MailAddress("dudledok@gmail.com");
        mail.To.Add("alexander@rewind.co");
        mail.Subject = "Barclays LifeSkills - Work Experience Simulator";
        mail.Body = bodyText;


        SmtpClient client = new SmtpClient("smtp.gmail.com");
        client.Port = 587;
        client.Credentials = new System.Net.NetworkCredential("dudledok@gmail.com", "password") as ICredentialsByHost;
        client.EnableSsl = true;
        ServicePointManager.ServerCertificateValidationCallback =
          delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
          { return true; };
        try
        {
            client.SendAsync(mail, "");
            Debug.Log("Success, email sent through SMTP!");
        }
        catch (Exception ex)
        {
            Debug.Log(string.Format("Exception caught in CreateMessageWithAttachment(): {0}", ex.ToString()));
        }
    }
}