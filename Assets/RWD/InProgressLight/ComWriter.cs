using UnityEngine;
using System;
using System.IO.Ports;
using System.Threading;
using System.Collections;
using System.Text.RegularExpressions;

//Supported baud rates
enum BR
{
    BR_9600 = 9600,
    BR_14400 = 14400,
    BR_19200 = 19200,
    BR_28800 = 28800,
    BR_38400 = 38400,
    BR_57600 = 57600,
    BR_115200 = 115200
}

public class ComWriter : MonoSingleton<ComWriter>
{

    [SerializeField]
    string PortName;

    [SerializeField]
    BR BaudRate;

    private SerialPort serialPort;
    private Thread readThread;
    public string data { get; private set; }

    void Awake()
    {
        Instance = this;
    }

    public void ToggleLight(bool _on)
    {
        if(_on)
            serialPort.WriteLine("1");
        else
            serialPort.WriteLine("0");
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.H))
            ToggleLight(true);
        if (Input.GetKeyDown(KeyCode.I))
            ToggleLight(false);
    }

    // Use this for initialization
    void Start()
    {
        data = "";

        // Create a new SerialPort object with default settings.
        serialPort = new SerialPort();

        // Allow the user to set the appropriate properties.
        serialPort.PortName = PortName;
        serialPort.BaudRate = (int)BaudRate;
        serialPort.Parity = Parity.None;
        serialPort.DataBits = 8;
        serialPort.StopBits = StopBits.One;
        serialPort.Handshake = Handshake.None;

        // Set the read/write timeouts
        serialPort.ReadTimeout = 2000;
        serialPort.WriteTimeout = 2000;

        serialPort.Open();
    }

    void OnDestroy()
    {
        ToggleLight(false);
    }
}