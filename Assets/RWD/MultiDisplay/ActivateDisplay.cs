﻿using UnityEngine;
using System.Collections;

public class ActivateDisplay : MonoBehaviour
{
	void Awake ()
    {
        for (int i = 0; i < Display.displays.Length; i++)
        {
            if (Display.displays[i] != null)
            {
                Display.displays[i].Activate(Display.displays[i].systemWidth, Display.displays[i].systemHeight, 60);
            }
        }
    }
}