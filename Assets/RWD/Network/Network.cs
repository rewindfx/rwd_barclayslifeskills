﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;
using System.Collections;

public class Network : NetworkManager
{
    NetworkClient myClient;

    [SerializeField]
    string ip;

    void Start()
    {
        NetworkServer.Listen(4444);
        myClient = new NetworkClient();
        myClient.RegisterHandler(MsgType.Connect, OnConnected);
        NetworkServer.RegisterHandler(MyBeginMsg, OnServerReadyToBeginMessage);
        myClient.Connect(ip, 4444);
    }

    public void OnConnected(NetworkMessage netMsg)
    {
        Debug.Log("OnConnected");
    }

    public override void OnServerConnect(NetworkConnection conn)
    {
        Debug.Log("OnServerConnect");
    }

    void OnClientConnect(NetworkMessage msg)
    {
        Debug.Log("OnClientConnect");
    }


    const short MyBeginMsg = 1002;

    void OnServerReadyToBeginMessage(NetworkMessage netMsg)
    {
        UserDetailsMessage userMessage = netMsg.ReadMessage<UserDetailsMessage>();
        User newUser = new User();
        newUser.Name = userMessage.Name;
        newUser.Email = userMessage.Email;
        newUser.Sector = userMessage.Sector;
        newUser.FullLength = userMessage.FullLength;
        UserListManager.Instance.AddUser(newUser);
        Debug.Log(string.Format("User: {0} - Email: {1} - Sector: {2} - Full Length Experience: {3}", userMessage.Name, userMessage.Email, userMessage.Sector, userMessage.FullLength));
    }
}

public class UserDetailsMessage : MessageBase
{
    public string Name;
    public string Email;
    public WorkSector Sector;
    public bool FullLength;
}