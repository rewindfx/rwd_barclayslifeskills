﻿using UnityEngine;
using System.Collections;

public class ActivityPrefab : MonoBehaviour
{
    [SerializeField]
    DraggableActivity activity;
    public DraggableActivity Activity
    {
        get { return activity; }
    }
}