﻿using UnityEngine;
using System.Collections;
using TMPro;

public class DraggableActivity : MonoBehaviour
{
    [SerializeField]
    TextMeshProUGUI description;
    public TextMeshProUGUI Description
    {
        get { return description; }
        set { description = value; }
    }

    Department department;
    public Department Department
    {
        get { return department; }
    }

    public void Initialise(string _description, Department _department)
    {
        description.text = _description;
        department = _department;
    }
}