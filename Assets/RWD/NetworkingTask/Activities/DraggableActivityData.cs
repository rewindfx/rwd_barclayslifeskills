﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(fileName = "Activity", menuName = "Barclays/DraggableActivity", order = 3)]
public class DraggableActivityData : ScriptableObject
{
    [SerializeField]
    Department department;
    public Department Department
    {
        get { return department; }
    }
    [SerializeField]
    string description;
    public string Description
    {
        get { return description; }
    }
}