﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using TMPro;

public class Employee : MonoBehaviour
{
    [SerializeField]
    TextMeshProUGUI employeeName;

    [SerializeField]
    Image headshot;

    Department department;
    [SerializeField]
    TextMeshProUGUI departmentText;

    [SerializeField]
    TextMeshProUGUI descriptionText;

    [SerializeField]
    DraggableActivity[] dropTargets;

    public void Initialise(string _name, Sprite _headshot, Department _department, string _description)
    {
        employeeName.text = _name;
        headshot.sprite = _headshot;
        department = _department;
        departmentText.text = AddSpacesToSentence(department.ToString());
        descriptionText.text = _description;
    }

    string AddSpacesToSentence(string text)
    {
        if (string.IsNullOrEmpty(text))
            return "";
        string newText = "";
        newText += text[0];
        newText += text[1];
        for (int i = 2; i < text.Length; i++)
        {
            if (char.IsUpper(text[i]) && text[i - 1] != ' ')
                newText+= " ";
            newText += text[i];
        }
        return newText;
    }

    public bool IsValid(ref int _activityCount)
    {
        bool isValid = true;
        foreach(DraggableActivity activity in dropTargets)
        {
            if (!string.IsNullOrEmpty(activity.Description.text))
            {
                ++_activityCount;
                if (activity.Department != department)
                {
                    isValid = false;
                }
            }
        }
        return isValid;
    }
}