﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(fileName = "Employee", menuName = "Barclays/Employee", order = 2)]
public class EmployeeData : ScriptableObject
{
    [SerializeField]
    string employeeName;
    public string Name
    {
        get { return employeeName; }
    }
    [SerializeField]
    Department department;
    public Department Department
    {
        get { return department; }
    }
    [SerializeField]
    string description;
    public string Description
    {
        get { return description; }
    }
    [SerializeField]
    Sprite headshot;
    public Sprite Headshot
    {
        get { return headshot; }
    }
}