﻿using UnityEngine;
using System.Collections;

public class Popup : MonoBehaviour
{
    [SerializeField]
    TMPro.TextMeshProUGUI message;

    public void SetText(string _text)
    {
        message.text = _text;
    }
}