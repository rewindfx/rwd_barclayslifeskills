﻿using System.Collections;

public class FSM<T>
{
    //T owner;

    State<T> currentState;
    public State<T> CurrentState
    {
        get { return currentState; }
    }

    public FSM()//T _owner)
    {
        //owner = _owner;
        currentState = null;
    }
    
    public void Initialise(State<T> _state)
    {
        currentState = _state;
        currentState.Enter();
    }

    public readonly Condition Condition;

    public void Update()
    {
        if (currentState == null)
            return;

        if (currentState.Transitions.Count == 0)
        {
            if (currentState.IsFinished)
            {
                currentState.Execute();
                currentState.Exit();
                currentState = null;
            }
            else
            {
                currentState.Execute();
            }
        }
        else
        {
            foreach (Transition<T> _transition in currentState.Transitions)
            {
                if (_transition.Condition())
                {
                    currentState.Exit();
                    currentState = _transition.NextState;
                    currentState.Enter();
                }
            }
            currentState.Execute();
        }
    }
}