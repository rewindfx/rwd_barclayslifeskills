﻿using System.Collections;
using System.Collections.Generic;

public abstract class State<T>
{
	public abstract void Enter();
	public abstract void Exit();
	public abstract void Execute();

	public string Name { get; set; }

    protected bool isFinished;
    public bool IsFinished
    {
        get { return isFinished; }
        set { isFinished = value; }
    }

    private List<Transition<T>> transitions = new List<Transition<T>> ();
	public List<Transition<T>> Transitions
	{
		get { return transitions; }
	}

	public void AddTransition(Transition<T> _transition)
	{
		transitions.Add (_transition);
	}
}

public delegate bool Condition();

public class Transition<T>
{
	public readonly State<T> NextState;
	public readonly Condition Condition;

	public Transition(State<T> _nextState, Condition _condition)
	{
		NextState = _nextState;
		Condition += _condition;
	}
}