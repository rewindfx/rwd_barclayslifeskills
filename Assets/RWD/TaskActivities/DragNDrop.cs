﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public enum Department
{
    Finance,
    HumanResources,
    Marketing,
    Digital,
    Sales,
    IT
}

public class DragNDrop : TaskActivity
{
    [Header("Draggables")]
    [SerializeField]
    Transform activityGrid;

    [SerializeField]
    ActivityPrefab activityPrefab;

    [SerializeField]
    DraggableActivityData[] activities;

    [Header("Targets")]
    [SerializeField]
    Transform employeeGrid;

    [SerializeField]
    Employee employeePrefab;

    [SerializeField]
    EmployeeData[] employeeData;

    List<Employee> employees;
    
    void Start()
    {
        employees = new List<Employee>();

        foreach(DraggableActivityData activity in activities)
        {
            ActivityPrefab prefab = GameObject.Instantiate(activityPrefab);
            prefab.name = activity.name;
            prefab.transform.SetParent(activityGrid);
            prefab.transform.localScale = Vector3.one;
            prefab.transform.localPosition = Vector3.zero;
            prefab.Activity.Initialise(activity.Description, activity.Department);
        }
        foreach (EmployeeData employee in employeeData)
        {
            Employee e = GameObject.Instantiate(employeePrefab);
            e.name = employee.name;
            e.transform.SetParent(employeeGrid);
            e.transform.localScale = Vector3.one;
            e.transform.localPosition = Vector3.zero;
            e.Initialise(employee.Name, employee.Headshot, employee.Department, employee.Description);
            employees.Add(e);
        }
    }

    public void ActivityComplete()
    {
        bool activityComplete = true;
        int activityCount = 0;
        foreach(Employee employee in employees)
        {
            if(!employee.IsValid(ref activityCount))
            {
                activityComplete = false;
                break;
            }
        }
        if (activityCount < activities.Length)
            activityComplete = false;

        SetResponseValue(activityComplete);
    }
}