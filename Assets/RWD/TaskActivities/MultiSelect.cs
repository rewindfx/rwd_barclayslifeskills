﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using TMPro;

public class MultiSelect : TaskActivity
{
    [SerializeField]
    Toggle[] correctToggles;

    [SerializeField]
    Toggle[] incorrectToggles;

    ResponseValue response;

    int correctTweetCount;
    int incorrectTweetCount;

    void Start()
    {
        correctTweetCount = 0;
        incorrectTweetCount = 0;
    }

    public void CheckInput()
    {
        int incorrectCount = 0;
        foreach(Toggle toggle in incorrectToggles)
        {
            if (toggle.isOn)
                ++incorrectCount;
        }
        int correctCount = 0;
        foreach (Toggle toggle in correctToggles)
        {
            if (toggle.isOn)
                ++correctCount;
        }
        if(incorrectCount + correctCount == 2)
        {
            if (correctCount == 2)
                response = ResponseValue.Correct;
            else if (correctCount == 1)
                response = ResponseValue.Almost;
        }
        else
        {
            response = ResponseValue.Incorrect;
        }
    }

    public void PostTweet(bool _correct)
    {
        if (_correct)
            ++correctTweetCount;
        else
            ++incorrectTweetCount;

        if (incorrectTweetCount + correctTweetCount == 2)
        {
            if (correctTweetCount == 2)
                response = ResponseValue.Correct;
            else if (correctTweetCount == 1)
                response = ResponseValue.Almost;
            Submit();
        }
        else
        {
            response = ResponseValue.Incorrect;
            UIManager.Instance.ShowPopupWithText("Always remember that social media is public, so anyone can see what you write! Ask yourself, is this something you would want your current or future employers to read?");
        }
    }

    public void Submit()
    {
        //if (response == ResponseValue.Unanswered)
        //    response = ResponseValue.Incorrect;
        SetResponseValue(response);
    }
}