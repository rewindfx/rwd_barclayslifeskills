﻿using UnityEngine;
using System.Collections;

public class NetworkingBigScreen : MonoBehaviour
{
    [SerializeField]
    Task task;

    [Header("Targets")]
    [SerializeField]
    Transform employeeGrid;

    [SerializeField]
    Employee employeePrefab;

    [SerializeField]
    EmployeeData[] employeeData;

    void Start()
    {
        foreach (EmployeeData employee in employeeData)
        {
            Employee e = GameObject.Instantiate(employeePrefab);
            e.name = employee.name;
            e.transform.SetParent(employeeGrid);
            e.transform.localScale = Vector3.one;
            e.transform.localPosition = Vector3.zero;
            e.transform.localRotation = Quaternion.identity;
            e.Initialise(employee.Name, employee.Headshot, employee.Department, employee.Description);
        }
    }
}