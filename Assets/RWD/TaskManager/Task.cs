﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;


[CreateAssetMenu(fileName = "Task", menuName = "Barclays/Task", order = 1)]
public class Task : ScriptableObject
{
    [SerializeField]
    TaskType type;
    public TaskType Type
    {
        get { return type; }
    }
    [SerializeField]
    float timeLimit;
    public float TimeLimit
    {
        get { return timeLimit; }
    }
    [SerializeField]
    Sprite background;
    public Sprite Background
    {
        get { return background; }
    }
    [SerializeField]
    Color color;
    public Color Color
    {
        get { return color; }
    }
    [SerializeField]
    GameObject menu;
    public GameObject Menu
    {
        get { return menu; }
    }
    [SerializeField]
    GameObject screenMenu;
    public GameObject ScreenMenu
    {
        get { return screenMenu; }
    }
    [SerializeField]
    string correctResponse;
    public string CorrectResponse
    {
        get { return correctResponse; }
    }
    [SerializeField]
    string almostCorrectResponse;
    public string AlmostCorrectResponse
    {
        get { return almostCorrectResponse; }
    }
    [SerializeField]
    string incorrectResponse;
    public string IncorrectResponse
    {
        get { return incorrectResponse; }
    }
    [SerializeField]
    string unansweredResponse;
    public string UnansweredResponse
    {
        get { return unansweredResponse; }
    }
    ResponseValue response;
    public ResponseValue Response
    {
        get { return response; }
    }

    public void SetResponseValue(ResponseValue _response)
    {
        response = _response;
    }

    public bool ActivityCompleted()
    {
        if (response == ResponseValue.Unanswered)
            return false;
        else
            return true;
    }
}

public class TaskState : State<TaskManager>
{
    Task task;
    public Task Task
    {
        get { return task; }
    }

    FSM<TaskState> fsm;

    //bool finalState;
    //public bool FinalState
    //{
    //    get { return finalState; }
    //    set { finalState = value; }
    //}

    public TaskState(Task _task)
    {
        task = _task;
    }

    public override void Enter()
    {
        fsm = new FSM<TaskState>();

        //Debug.Log(task.name);

        isFinished = false;

        IntroVideo introVideo = new IntroVideo(this);
        Activity activity = new Activity(this);
        Response response = new Response(this);

        introVideo.AddTransition(new Transition<TaskState>(activity, VideoManager.Instance.MovieFinished));
        activity.AddTransition(new Transition<TaskState>(response, () => activity.IsFinished));
        activity.AddTransition(new Transition<TaskState>(response, task.ActivityCompleted));

        fsm.Initialise(introVideo);
    }

    public override void Execute()
    {
        fsm.Update();
    }

    public override void Exit()
    {
    }
}

public class IntroVideo : State<TaskState>
{
    TaskState owner;
    public IntroVideo(TaskState _owner)
    {
        owner = _owner;
    }

    public override void Enter()
    {
        BigScreenManager.Instance.SwitchScreen(true, owner.Task);
        UIManager.Instance.ChangeToIntro(owner.Task.Background);
        VideoManager.Instance.QueueIntroVideo(owner, TaskManager.Instance.Sector);
        Debug.Log(string.Format("Intro video for task {0}", owner.Task.Type.ToString()));
    }

    public override void Execute()
    {
    }

    public override void Exit()
    {
    }
}

public class Activity : State<TaskState>
{
    TaskState owner;

    public Activity(TaskState _owner)
    {
        owner = _owner;
        isFinished = false;
    }

    public override void Enter()
    {
        UIManager.Instance.SetTimer(owner.Task.TimeLimit);
        UIManager.Instance.InstantiateScreen(owner.Task);
        BigScreenManager.Instance.SwitchScreen(false, owner.Task);
        Debug.Log(string.Format("Activity for task {0}", owner.Task.Type.ToString()));
    }

    public override void Execute()
    {
        if(UIManager.Instance.Timer < 0f)
        {
            isFinished = true;
        }
    }

    public override void Exit()
    {
    }
}

public class Response : State<TaskState>
{
    TaskState owner;

    public Response(TaskState _owner)
    {
        owner = _owner;
    }

    public override void Enter()
    {
        UIManager.Instance.SetTimer(10f);//30s
        UIManager.Instance.ChangeToResponse(owner.Task);
        BigScreenManager.Instance.RemoveMenu(); //TODO Change this? Instantiating a big screen menu will do this
        Debug.Log(string.Format("Response for task {0}", owner.Task.Type.ToString()));
    }

    public override void Execute()
    {
        if (UIManager.Instance.Timer < 0f)
        {
            owner.IsFinished = true;
            isFinished = true;
        }
    }

    public override void Exit()
    {
        Debug.Log(string.Format("Exiting {0}", owner.Task.Type.ToString()));
        EmailManager.Instance.SendEmail(DataManager.Instance.ReadActivities());
        UIManager.Instance.ChangeToFeedback(DataManager.Instance.ReadActivities());
        //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}