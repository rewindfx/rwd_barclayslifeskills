﻿using UnityEngine;
using System.Collections;

public class TaskActivity : MonoBehaviour
{
    [SerializeField]
    protected Task task;

    public void SetResponseValue(ResponseValue _response)
    {
        task.SetResponseValue(_response);
        DataManager.Instance.RecordActivity(task);
    }

    public void SetResponseValue(bool _correct)//because buttons can't take enums as a parameter in the inspector
    {
        if (_correct)
            SetResponseValue(ResponseValue.Correct);
        else
            SetResponseValue(ResponseValue.Incorrect);
    }
}