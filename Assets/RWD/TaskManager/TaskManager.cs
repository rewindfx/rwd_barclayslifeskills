﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public enum WorkSector
{
    Retail,
    Business,
    Administration,
    Health,
    Manufacturing
}

[System.Serializable]
public enum TaskType
{
    Networking,
    Resilience,
    DecisionMaking,
    Professionalism
}

[System.Serializable]
public enum ResponseValue
{
    Unanswered = 0,
    Incorrect = 1,
    Correct = 2,
    Almost = 3
}

public class TaskManager : MonoSingleton<TaskManager>
{
    FSM<TaskManager> fsm;

    WorkSector sector;
    public WorkSector Sector
    {
        get { return sector; }
    }

    [SerializeField]
    Task networkingTask;
    [SerializeField]
    Task resilienceTask;
    [SerializeField]
    Task decisionMakingTask;
    [SerializeField]
    Task professionalismTask;

    void Awake()
    {
        Instance = this;
    }

    void ResetTaskResponses()
    {
        networkingTask.SetResponseValue(ResponseValue.Unanswered);
        resilienceTask.SetResponseValue(ResponseValue.Unanswered);
        decisionMakingTask.SetResponseValue(ResponseValue.Unanswered);
        professionalismTask.SetResponseValue(ResponseValue.Unanswered);
    }
    
    public void Initialise(bool _fullLength) // called from lite/full button
    {
        ResetTaskResponses();

        fsm = new FSM<TaskManager>();

        sector = UserListManager.Instance.currentUser.Sector;

        TaskState resiliance = new TaskState(resilienceTask);
        TaskState professionalism = new TaskState(professionalismTask);

        if (_fullLength)
        {
            TaskState networking = new TaskState(networkingTask);
            TaskState decisionMaking = new TaskState(decisionMakingTask);

            networking.AddTransition(new Transition<TaskManager>(resiliance, () => networking.IsFinished));
            resiliance.AddTransition(new Transition<TaskManager>(decisionMaking, () => resiliance.IsFinished));
            decisionMaking.AddTransition(new Transition<TaskManager>(professionalism, () => decisionMaking.IsFinished));

            fsm.Initialise(networking);
        }
        else
        {
            resiliance.AddTransition(new Transition<TaskManager>(professionalism, () => resiliance.IsFinished));

            fsm.Initialise(resiliance);
        }

        ComWriter.Instance.ToggleLight(true);
    }

    void Update()
    {
        if(fsm != null)
            fsm.Update();
    }
}