﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(Image))]
public class SelectRandomImage : MonoBehaviour
{
    [SerializeField]
    Sprite[] images;

    Image image;
    
    void Start()
    {
        int randomIndex = Random.Range(0, images.Length);
        image = GetComponent<Image>();
        image.sprite = images[randomIndex];
    }
}