﻿using UnityEngine;
using System.Collections;

public class BigScreenManager : MonoSingleton<BigScreenManager>
{
    GameObject currentMenu;

    [SerializeField]
    RectTransform UIScreen;

    [SerializeField]
    GameObject videoScreen;
    public GameObject VideoScreen
    {
        get { return videoScreen; }
    }

    [SerializeField]
    GameObject secondScreenParent;

    void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        Vector2 size = UIScreen.sizeDelta;
        size.x = -Screen.width + 30f;
        UIScreen.sizeDelta = size;
        currentMenu = null;
    }

    public void SwitchScreen(bool _switchToVideo, Task _task)
    {

        if (_switchToVideo)
        {
            if (!currentMenu)
            {
                print("1");
                StartCoroutine(AnimateOpen(videoScreen, true));
            }
            else
            {
                print("2");
                StartCoroutine(AnimateClosed(secondScreenParent, true));
            }
        }
        else
        {
            if (!currentMenu)
            {
                print("3");
                StartCoroutine(AnimateOpen(secondScreenParent, true));
                if (_task.ScreenMenu)
                    InstantiateScreen(_task);
            }
            else
            {
                if (currentMenu == videoScreen)
                {
                    print("4");
                    if (_task.ScreenMenu)
                    {
                        InstantiateScreen(_task);
                        CloseVideo();
                    }
                    else
                    {
                        StartCoroutine(AnimateClosed(videoScreen, false));
                    }
                }
                else
                {
                    print("5");
                }
            }
        }
    }

    public void InstantiateScreen(Task _task)
    {
        RemoveMenu();
        GameObject menu = (GameObject)GameObject.Instantiate(_task.ScreenMenu);
        menu.transform.SetParent(secondScreenParent.transform);
        menu.transform.localRotation = Quaternion.identity;
        menu.transform.localPosition = Vector3.zero;
        menu.transform.localScale = Vector3.one;
        menu.GetComponent<RectTransform>().sizeDelta = new Vector2(-200f, -200f);
    }

    public void RemoveMenu()
    {
        foreach (Transform t in secondScreenParent.transform)
            Destroy(t.gameObject);
    }

    void CloseVideo()
    {
        videoScreen.SetActive(false);
        secondScreenParent.SetActive(true);
    }

    IEnumerator AnimateClosed(GameObject _menu, bool _openOther)
    {
        Vector2 size = UIScreen.sizeDelta;
        size.x = -30f;
        while (size.x > -Screen.width + 30f)
        {
            size.x -= 1700f * Time.deltaTime;
            UIScreen.sizeDelta = size;
            yield return null;
        }
        size.x = -Screen.width + 30f;
        UIScreen.sizeDelta = size;
        _menu.SetActive(false);
        currentMenu = null;
        if (_openOther)
        {
            if (_menu == videoScreen)
                StartCoroutine(AnimateOpen(secondScreenParent, false));
            else
                StartCoroutine(AnimateOpen(videoScreen, false));
        }
    }

    IEnumerator AnimateOpen(GameObject _menu, bool _delay)
    {
        if (_delay)
            yield return new WaitForSeconds(1.1f);
        currentMenu = _menu;
        _menu.SetActive(true);
        Vector2 size = UIScreen.sizeDelta;
        size.x = -Screen.width;
        while (size.x < -30f)
        {
            size.x += 1700f * Time.deltaTime;
            UIScreen.sizeDelta = size;
            yield return null;
        }
        size.x = -30f;
        UIScreen.sizeDelta = size;
    }

    //IEnumerator AnimateVideoClosed(bool _switchToVideo)
    //{
    //    Vector2 size = UIScreen.sizeDelta;
    //    size.x = -30f;
    //    while (size.x > -Screen.width)
    //    {
    //        size.x -= 1700f * Time.deltaTime;
    //        UIScreen.sizeDelta = size;
    //        yield return null;
    //    }
    //    size.x = -Screen.width;
    //    UIScreen.sizeDelta = size;
    //    videoScreen.SetActive(false);
    //    currentMenu = null;
    //    if (_switchToVideo)
    //        StartCoroutine(AnimateVideoOpen());
    //    else
    //        StartCoroutine(AnimateOtherOpen());
    //}

    //IEnumerator AnimateVideoOpen()
    //{
    //    currentMenu = videoScreen;
    //    videoScreen.SetActive(true);
    //    Vector2 size = UIScreen.sizeDelta;
    //    size.x = -Screen.width;
    //    while (size.x < -30f)
    //    {
    //        size.x += 1700f * Time.deltaTime;
    //        UIScreen.sizeDelta = size;
    //        yield return null;
    //    }
    //    size.x = -30f;
    //    UIScreen.sizeDelta = size;
    //}

    //IEnumerator AnimateOtherClosed(bool _switchToVideo)
    //{
    //    Vector2 size = UIScreen.sizeDelta;
    //    size.x = -30f;
    //    while (size.x > -Screen.width)
    //    {
    //        size.x -= 1700f * Time.deltaTime;
    //        UIScreen.sizeDelta = size;
    //        yield return null;
    //    }
    //    size.x = -Screen.width;
    //    UIScreen.sizeDelta = size;
    //    secondScreenParent.SetActive(false);
    //    currentMenu = null;
    //    if (_switchToVideo)
    //        StartCoroutine(AnimateVideoOpen());
    //    else
    //        StartCoroutine(AnimateOtherOpen());
    //}

    //IEnumerator AnimateOtherOpen()
    //{
    //    currentMenu = secondScreenParent;
    //    secondScreenParent.SetActive(true);
    //    Vector2 size = UIScreen.sizeDelta;
    //    size.x = -Screen.width;
    //    while (size.x < -30f)
    //    {
    //        size.x += 1700f * Time.deltaTime;
    //        UIScreen.sizeDelta = size;
    //        yield return null;
    //    }
    //    size.x = -30f;
    //    UIScreen.sizeDelta = size;
    //}
}