﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using TMPro;

public class UIManager : MonoSingleton<UIManager>
{
    [SerializeField]
    Sprite cyanBackground;

    [SerializeField]
    TextMeshProUGUI clockText;
    float timer;
    public float Timer
    {
        get { return timer; }
    }

    [SerializeField]
    Popup popup;

    [SerializeField]
    RectTransform UIScreen;
    [SerializeField]
    Transform menusParent;
    [SerializeField]
    Image chalkboard;

    [SerializeField]
    GameObject currentMenu;

    [SerializeField]
    GameObject introVideoMenu;

    [SerializeField]
    GameObject responseMenu;
    [SerializeField]
    TextMeshProUGUI responseText;

    [SerializeField]
    GameObject feedbackMenu;
    [SerializeField]
    TextMeshProUGUI feedbackText;

    bool pauseTimer;

    void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        pauseTimer = false;
        StartCoroutine(AnimateOpen());
    }

    public void ChangeScreenAnimated(GameObject _menu)
    {
        StartCoroutine(AnimateClosed(_menu));
    }

    public void ChangeScreen(GameObject _menu)
    {
        currentMenu.SetActive(false);
        currentMenu = _menu;
        currentMenu.SetActive(true);
    }

    IEnumerator AnimateClosed(GameObject _menu)
    {
        Vector2 size = UIScreen.sizeDelta;
        size.x = -30f;
        while (size.x > -Screen.width)
        {
            size.x -= 1700f * Time.deltaTime;
            UIScreen.sizeDelta = size;
            yield return null;
        }
        size.x = -Screen.width;
        UIScreen.sizeDelta = size;
        currentMenu.SetActive(false);
        currentMenu = _menu;
        currentMenu.SetActive(true);
        StartCoroutine(AnimateOpen());
    }

    IEnumerator AnimateClosed(GameObject _menu, Color _color)
    {
        Vector2 size = UIScreen.sizeDelta;
        size.x = -30f;
        while (size.x > -Screen.width + 30f)
        {
            size.x -= 1700f * Time.deltaTime;
            UIScreen.sizeDelta = size;
            yield return null;
        }
        size.x = -Screen.width + 30f;
        UIScreen.sizeDelta = size;
        currentMenu.SetActive(false);
        currentMenu = _menu;
        SetChalkboardColour(_color);
        currentMenu.SetActive(true);
        StartCoroutine(AnimateOpen());
    }

    IEnumerator AnimateClosed(GameObject _menu, Sprite _sprite)
    {
        Vector2 size = UIScreen.sizeDelta;
        size.x = -30f;
        while (size.x > -Screen.width + 30f)
        {
            size.x -= 1700f * Time.deltaTime;
            UIScreen.sizeDelta = size;
            yield return null;
        }
        size.x = -Screen.width + 30f;
        UIScreen.sizeDelta = size;
        currentMenu.SetActive(false);
        currentMenu = _menu;
        SetChalkboardBackground(_sprite);
        currentMenu.SetActive(true);
        StartCoroutine(AnimateOpen());
    }

    IEnumerator AnimateOpen()
    {
        Vector2 size = UIScreen.sizeDelta;
        size.x = -Screen.width;
        while (size.x < -30f)
        {
            size.x += 1700f * Time.deltaTime;
            UIScreen.sizeDelta = size;
            yield return null;
        }
        size.x = -30f;
        UIScreen.sizeDelta = size;
    }

    void SetChalkboardColour(Color _color)
    {
        chalkboard.color = _color;
    }

    void SetChalkboardBackground(Sprite _sprite)
    {
        chalkboard.sprite = _sprite;
    }

    public void InstantiateScreen(Task _task)
    {
        GameObject menu = (GameObject)GameObject.Instantiate(_task.Menu);
        menu.transform.SetParent(menusParent);
        menu.transform.SetAsFirstSibling();
        menu.transform.localRotation = Quaternion.identity;
        menu.transform.localPosition = Vector3.zero;
        menu.transform.localScale = Vector3.one;
        menu.GetComponent<RectTransform>().sizeDelta = Vector2.zero;
        ChangeScreen(menu);
    }

    public void ChangeToIntro(Sprite _sprite)
    {
        StartCoroutine(AnimateClosed(introVideoMenu, _sprite));
    }

    public void ChangeToResponse(Task _task)
    {
        switch(_task.Response)
        {
            case ResponseValue.Correct:
                responseText.text = _task.CorrectResponse;
                break;
            case ResponseValue.Almost:
                responseText.text = _task.AlmostCorrectResponse;
                break;
            case ResponseValue.Incorrect:
                responseText.text = _task.IncorrectResponse;
                break;
            case ResponseValue.Unanswered:
                responseText.text = _task.UnansweredResponse;
                break;
        }
        ChangeScreen(responseMenu);
    }

    public void ChangeToFeedback(List<Task> _tasks)
    {
        Debug.Log("Change to feedback");
        StartCoroutine(AnimateClosed(feedbackMenu, cyanBackground));
        
        if (_tasks.Count == 0)
        {
            Debug.Log("No tasks recorded");
            return;
        }
        string bodyText = "";
        foreach (Task t in _tasks)
        {
            bodyText += t.Type.ToString();
            bodyText += "\n";
            switch (t.Response)
            {
                case ResponseValue.Correct:
                    bodyText += t.CorrectResponse;
                    break;

                case ResponseValue.Almost:
                    bodyText += t.AlmostCorrectResponse;
                    break;

                case ResponseValue.Incorrect:
                    bodyText += t.IncorrectResponse;
                    break;

                case ResponseValue.Unanswered:
                    bodyText += t.UnansweredResponse;
                    break;
            }
            bodyText += "\n\n";
        }
        bodyText += "Thank you etc. \n Signed Barclays";

        feedbackText.text = bodyText;
    }

    public void ShowPopupWithText(string _text)
    {
        popup.SetText(_text);
        StartCoroutine(Popup());
    }

    IEnumerator Popup()
    {
        pauseTimer = true;
        popup.gameObject.SetActive(true);
        yield return new WaitForSeconds(8f);
        popup.gameObject.SetActive(false);
        pauseTimer = false;
    }

    public void SetTimer(float _time)
    {
        clockText.gameObject.SetActive(false);
        timer = _time;
        StartCoroutine(SetTimerAfterDelay(_time));
    }

    IEnumerator SetTimerAfterDelay(float _time)
    {
        yield return new WaitForSeconds(1f);
        clockText.gameObject.SetActive(true);
    }

    void Update()
    {
        if (!pauseTimer && timer > 0f)
        {
            timer -= Time.deltaTime;
            int minuteCount = 0;
            float secondCount = timer;
            while(secondCount > 59f)
            {
                secondCount -= 60f;
                ++minuteCount;
            }
            clockText.text = string.Format("{0}:{1}", minuteCount.ToString("00"), secondCount.ToString("00"));
        }

        if (timer < 0f)
        {
            clockText.gameObject.SetActive(false);
        }
    }
}