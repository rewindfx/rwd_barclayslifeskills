﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using TMPro;

[RequireComponent(typeof(Button))]
public class UserButton : MonoBehaviour
{
    [SerializeField]
    TextMeshProUGUI username;

    public User user;

    Button button;

    Vector3 startPos;

    void Start()// comment out when using tablet app
    {
        if (this.name == "User Button (1)")
        {
            user = new User();
            user.Name = "peter";
            user.Email = "alexander@rewind.co";
            user.Sector = WorkSector.Business;
            user.FullLength = true;
        }
        Init();
        startPos = transform.position;
    }

    public void Init()
    {
        button = GetComponent<Button>();
        username.text = user.Name;
        button.onClick.AddListener(new UnityEngine.Events.UnityAction(() => UserListManager.Instance.SetUser(this)));
    }

    public void OnBeginDrag()
    {
        startPos = Input.mousePosition;
    }

    public void OnEndDrag()
    {
        print(string.Format("Start: {0} - End: {1} - SqrMag: {2}", startPos, Input.mousePosition, (Input.mousePosition - startPos).sqrMagnitude));
        if((Input.mousePosition - startPos).sqrMagnitude > 700000f)
        {
            UserListManager.Instance.RemoveUser(user);
            Destroy(this.gameObject);
        }
    }

    public void DestroyAfterDelay()
    {
        Invoke("DestroyThis", 2f);
    }

    void DestroyThis()
    {
        Destroy(this.gameObject);
    }
}