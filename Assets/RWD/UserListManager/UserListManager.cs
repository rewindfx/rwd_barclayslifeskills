﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class UserListManager : MonoSingleton<UserListManager>
{
    [SerializeField]
    UserButton userPrefab;

    [SerializeField]
    Transform buttonParent;

    [SerializeField]
    GameObject nextScreen;

    public User currentUser;
    List<User> users;

    void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        users = new List<User>();
        Load();
    }

    public void AddUser(User _user)
    {
        users.Add(_user);
        Save();

        UserButton userGO = GameObject.Instantiate(userPrefab.gameObject).GetComponent<UserButton>();
        userGO.transform.SetParent(buttonParent.transform);
        userGO.transform.SetAsLastSibling();
        userGO.transform.localScale = Vector3.one;
        userGO.transform.localPosition = Vector3.zero;
        userGO.user = _user;
        userGO.Init();
    }

    public void RemoveUser(User _user)
    {
        users.Remove(_user);
        Save();
    }

    void Save()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/users.dat");
        bf.Serialize(file, users);
        file.Close();
    }

    void Load()
    {
        if (!File.Exists(Application.persistentDataPath + "/users.dat"))
            return;

        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Open(Application.persistentDataPath + "/users.dat", FileMode.Open);
        List<User> data = (List<User>)bf.Deserialize(file);
        file.Close();

        users = data;

        foreach(User user in users)
        {
            UserButton userGO = GameObject.Instantiate(userPrefab.gameObject).GetComponent<UserButton>();
            userGO.transform.SetParent(transform);
            userGO.transform.SetAsLastSibling();
            userGO.transform.localScale = Vector3.one;
            userGO.transform.localPosition = Vector3.zero;
            userGO.user = user;
            userGO.Init();
        }
    }

    public void SetUser(UserButton _userButton)
    {
        currentUser = _userButton.user;
        users.Remove(currentUser);
        Save();
        //UIManager.Instance.ChangeScreen(nextScreen);
        TaskManager.Instance.Initialise(_userButton.user.FullLength);
    }
}

[System.Serializable]
public class User
{
    public string Name;
    public string Email;
    public WorkSector Sector;
    public bool FullLength;
}