﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Video
{
    public string Path;
    public TaskType TaskType;
    public WorkSector Sector;
}

public class VideoManager : MonoSingleton<VideoManager>
{
    [SerializeField]
    Video[] videos;

    [SerializeField]
    AVProWindowsMediaMovie videoPlayer;

    void Awake()
    {
        Instance = this;
    }
    
    public void QueueIntroVideo(TaskState _taskState, WorkSector _sector)
    {
        //Debug.Log(string.Format("Task of type {0} and sector {1}", _task.Type.ToString(), _sector.ToString()));
        foreach(Video video in videos)
        {
            if(video.TaskType == _taskState.Task.Type && video.Sector == _sector)
            {
                QueueVideo(video.Path);
            }
        }
        videoPlayer.LoadMovie(true);
    }

    void QueueVideo(string _name)
    {
        videoPlayer._filename = _name;
    }

    public bool MovieFinished()
    {
        //return videoPlayer._moviePlayer.IsFinishedPlaying;
        if (videoPlayer._moviePlayer.DurationSeconds - videoPlayer._moviePlayer.PositionSeconds < 1f)
            return true;
        else
            return false;
    }
}