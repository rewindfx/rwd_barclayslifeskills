using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;

public class DragMe : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    GameObject objectDragging;
    Transform parent;
    int childPosition;
    CanvasGroup canvasgroup;

    void Start()
    {
        parent = transform.parent;
        childPosition = transform.GetSiblingIndex();
        canvasgroup = GetComponent<CanvasGroup>();
    }

	public void OnBeginDrag(PointerEventData eventData)
	{
        //Canvas canvas = FindInParents<Canvas>(gameObject);
        //if (canvas == null)
        //    return;

        //// We have clicked something that can be dragged.
        //// What we want to do is create an icon for this.
        //objectDragging = new GameObject("DraggingText");

        //objectDragging.transform.SetParent(canvas.transform, false);
        //objectDragging.transform.SetAsLastSibling();

        //TextMeshProUGUI tmp = objectDragging.AddComponent<TextMeshProUGUI>();
        //DraggableActivity newActivity = objectDragging.AddComponent<DraggableActivity>();
        //newActivity.Description = tmp;
        //DraggableActivity currentActivity = GetComponent<DraggableActivity>();
        //newActivity.Initialise(currentActivity.Description.text, currentActivity.Department);

        ////tmp.text = GetComponent<TextMeshProUGUI>().text;
        //tmp.alignment = TextAlignmentOptions.Center;
        //tmp.GetComponent<RectTransform>().sizeDelta = new Vector2(180, 180);
        //// The icon will be under the cursor.
        //// We want it to be ignored by the event system.
        //var group = objectDragging.AddComponent<CanvasGroup>();
        //group.blocksRaycasts = false;

        //SetDraggedPosition(eventData);
        transform.SetParent(UIManager.Instance.transform);

        SetDraggedPosition(eventData);
    }

	public void OnDrag(PointerEventData eventData)
	{
		//if (m_DraggingIcons[eventData.pointerId] != null)
		SetDraggedPosition(eventData);
	}

	private void SetDraggedPosition(PointerEventData eventData)
	{
		//var rt = m_DraggingIcons[eventData.pointerId].GetComponent<RectTransform>();
		transform.position = Camera.main.ScreenToWorldPoint(new Vector3(eventData.position.x, eventData.position.y, 100));
    }

	public void OnEndDrag(PointerEventData eventData)
	{
        //if (m_DraggingIcons[eventData.pointerId] != null)
        //	Destroy(m_DraggingIcons[eventData.pointerId]);

        //m_DraggingIcons[eventData.pointerId] = null;
        //Destroy(objectDragging);
        //objectDragging = null;
        canvasgroup.blocksRaycasts = false;
        RaycastHit hit;
        bool bhit = Physics.Raycast(transform.position - new Vector3(0f, 0f, 5f), Vector3.forward * 10f, out hit);
        //print(bhit);
        if (bhit)
        {
            //print(hit.transform.name);
            DropMe droptarget = hit.transform.GetComponent<DropMe>();
            if(droptarget)
            {
                if (!droptarget.IsEmpty())
                {
                    droptarget.Overwrite();
                }
                droptarget.SetActivity(GetComponent<DraggableActivity>(), gameObject);
                gameObject.SetActive(false);
            }
        }
        transform.SetParent(parent);
        transform.SetSiblingIndex(childPosition);
        canvasgroup.blocksRaycasts = true;
    }

	//static public T FindInParents<T>(GameObject go) where T : Component
	//{
	//	if (go == null) return null;
	//	var comp = go.GetComponent<T>();

	//	if (comp != null)
	//		return comp;
		
	//	var t = go.transform.parent;
	//	while (t != null && comp == null)
	//	{
	//		comp = t.gameObject.GetComponent<T>();
	//		t = t.parent;
	//	}
	//	return comp;
	//}
}
