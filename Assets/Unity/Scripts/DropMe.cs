using System.Reflection;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;

[RequireComponent(typeof(TextMeshProUGUI))]
public class DropMe : MonoBehaviour, IPointerDownHandler//, IDropHandler, IPointerEnterHandler, IPointerExitHandler
{
	//public Image containerImage;
    private TextMeshProUGUI receivingText;
	//private Color normalColor;
	//public Color highlightColor = Color.yellow;

    private DraggableActivity activity;

    GameObject associatedDraggable;
	
	public void OnEnable ()
	{
		//if (containerImage != null)
		//	normalColor = containerImage.color;

        receivingText = GetComponent<TextMeshProUGUI>();
        activity = GetComponent<DraggableActivity>();
	}

    public bool IsEmpty()
    {
        if (receivingText.text == "")
            return true;
        else
            return false;
    }

    public void Overwrite()
    {
        receivingText.text = "";
        activity = null;
        associatedDraggable.SetActive(true);
        associatedDraggable = null;
    }

    public void SetActivity(DraggableActivity _activity, GameObject _draggable)
    {
        associatedDraggable = _draggable;
        activity = GetComponent<DraggableActivity>();
        activity.Initialise(_activity.Description.text, _activity.Department);
    }

    //public void OnDrop(PointerEventData data)
    //   {
    //       activity = GetComponent<DraggableActivity>();
    //       containerImage.color = normalColor;

    //	if (receivingText == null)
    //		return;

    //       if (activity == null)
    //           return;


    //       var originalObj = data.pointerDrag;
    //       if (originalObj == null)
    //           return;

    //       var dragMe = originalObj.GetComponent<DraggableActivity>();
    //       if (dragMe == null)
    //           return;


    //       string dropText = dragMe.Description.text;
    //       if (!string.IsNullOrEmpty(dropText))
    //       {
    //           activity.Initialise(dropText, dragMe.Department);
    //       }
    //}

    //public void OnPointerEnter(PointerEventData data)
    //   {
    //       var originalObj = data.pointerDrag;
    //       if (originalObj == null)
    //           return;

    //	containerImage.color = highlightColor;
    //}

    //public void OnPointerExit(PointerEventData data)
    //{
    //	if (containerImage == null)
    //		return;

    //	containerImage.color = normalColor;
    //}

    public void OnPointerDown(PointerEventData data)
    {
        //print("click");
        Overwrite();
        //receivingImage.overrideSprite = emptyImage.sprite;
        //set text to empty
    }
}
